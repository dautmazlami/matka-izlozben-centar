/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradle.api.artifacts;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.Incubating;
import org.gradle.api.attributes.HasConfigurableAttributes;
import org.gradle.api.file.FileCollection;
import org.gradle.api.specs.Spec;
import org.gradle.api.tasks.TaskDependency;
import org.gradle.internal.HasInternalProtocol;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Map;
import java.util.Set;

import static groovy.lang.Closure.DELEGATE_FIRST;

/**
 * A {@code Configuration} represents a group of artifacts and their dependencies.
 * Find more information about declaring dependencies to a configuration
 * or about managing configurations in docs for {@link ConfigurationContainer}
 * <p>
 * Configuration is an instance of a {@link FileCollection}
 * that contains all dependencies (see also {@link #getAllDependencies()}) but not artifacts.
 * If you want to refer to the artifacts declared in this configuration
 * please use {@link #getArtifacts()} or {@link #getAllArtifacts()}.
 * Read more about declaring artifacts in the configuration in docs for {@link org.gradle.api.artifacts.dsl.ArtifactHandler}
 *
 * Please see the <a href="https://docs.gradle.org/current/userguide/managing_dependency_configurations.html" target="_top">Managing Dependency Configurations</a> User Manual chapter for more information.
 */
@HasInternalProtocol
public interface Configuration extends FileCollection, HasConfigurableAttributes<Configuration> {

    /**
     * Returns the resolution strategy used by this configuration.
     * The resolution strategy 