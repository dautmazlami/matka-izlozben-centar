package com.example.Matka.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Matka.R;
import com.example.Matka.adapter.HomeAdapter;
import com.example.Matka.model.EksponatModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    public static final String TAG = HomeFragment.class.getSimpleName();

    RecyclerView recyclerViewHome;
    HomeAdapter adapterHome;
    ArrayList<EksponatModel> eksponatList = new ArrayList<>();


    public static HomeFragment newInstance() {

        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        recyclerViewHome = view.findViewById(R.id.recyclerView_eksponati);
        recyclerViewHome.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        adapterHome = new HomeAdapter(getContext(),eksponatList);
        recyclerViewHome.setAdapter(adapterHome);
        loadEksponat();

        return view;
    }

    private void loadEksponat() {
        EksponatModel eksponatModel = new EksponatModel();
        eksponatModel.setEksponatImage(R.drawable.logo);
        eksponatModel.setEksponatTitle("Prv Eksponat");
        eksponatList.add(eksponatModel);

        EksponatModel eksponatModel2 = new EksponatModel();
        eksponatModel2.setEksponatImage(R.drawable.postafki);
        eksponatModel2.setEksponatTitle("vtor Eksponat");
        eksponatList.add(eksponatModel2);

        EksponatModel eksponatModel3 = new EksponatModel();
        eksponatModel3.setEksponatImage(R.drawable.postafki);
        eksponatModel3.setEksponatTitle("Tret Eksponat");
        eksponatList.add(eksponatModel3);

        EksponatModel eksponatModel4 = new EksponatModel();
        eksponatModel4.setEksponatImage(R.drawable.logo);
        eksponatModel4.setEksponatTitle("Prv Eksponat");
        eksponatList.add(eksponatModel4);


    }

}
