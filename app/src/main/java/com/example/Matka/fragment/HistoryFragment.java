package com.example.Matka.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Matka.R;
import com.example.Matka.adapter.HistoryAdapter;
import com.example.Matka.model.HistoryModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    public static final String TAG = HistoryFragment.class.getSimpleName();
    
    ArrayList<HistoryModel> historyList = new ArrayList<>();
    RecyclerView recyclerViewHistory;
    HistoryAdapter historyAdapter;


    public static HistoryFragment newInstance() {
        HistoryFragment historyFragment = new HistoryFragment();
        return historyFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        
        recyclerViewHistory = view.findViewById(R.id.recyclerView_History);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        historyAdapter = new HistoryAdapter(getContext(),historyList);
        recyclerViewHistory.setAdapter(historyAdapter);
        loadHistoryImage();
        return view;
    }

    private void loadHistoryImage() {
        HistoryModel historyModel = new HistoryModel();
        historyModel.setHistoryImage(R.drawable.istorija2);
        historyList.add(historyModel);

        HistoryModel historyModel2 = new HistoryModel();
        historyModel2.setHistoryImage(R.drawable.istorija3);
        historyList.add(historyModel2);

        HistoryModel historyModel3 = new HistoryModel();
        historyModel3.setHistoryImage(R.drawable.istorija4);
        historyList.add(historyModel3);

        HistoryModel historyModel4 = new HistoryModel();
        historyModel4.setHistoryImage(R.drawable.istorija5);
        historyList.add(historyModel4);
    }

}
