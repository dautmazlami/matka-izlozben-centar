package com.example.Matka.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Matka.R;
import com.example.Matka.adapter.VideoAdapter;
import com.example.Matka.model.VideoModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {

    public static final String TAG = VideoFragment.class.getSimpleName();

    RecyclerView recyclerViewVideo;
    VideoAdapter videoAdapter;
    ArrayList<VideoModel> videoList = new ArrayList<>();


    public static VideoFragment newInstance() {
        VideoFragment videoFragment = new VideoFragment();
        return videoFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_video, container, false);

        recyclerViewVideo = view.findViewById(R.id.recyclerView_Video);
        recyclerViewVideo.setLayoutManager(new GridLayoutManager(getActivity(), 2,GridLayoutManager.HORIZONTAL,false));
        videoAdapter = new VideoAdapter(getContext(),videoList);
        recyclerViewVideo.setAdapter(videoAdapter);
        loadEksponatVideo();
        return view;
    }

    private void loadEksponatVideo() {
        VideoModel videoModel = new VideoModel();
        videoModel.setEksponatImage(R.drawable.postafkiieksponati2);
        videoModel.setEskponatTilte("adaiosjdasiodjoasd");
        videoList.add(videoModel);

        VideoModel videoModel2 = new VideoModel();
        videoModel2.setEksponatImage(R.drawable.postafkiieksponati4);
        videoModel2.setEskponatTilte("adaiosjdasiodjoasd");
        videoList.add(videoModel2);

        VideoModel videoModel3 = new VideoModel();
        videoModel3.setEksponatImage(R.drawable.postafkiieksponati4);
        videoModel3.setEskponatTilte("adaiosjdasiodjoasd");
        videoList.add(videoModel3);

        VideoModel videoModel4 = new VideoModel();
        videoModel4.setEksponatImage(R.drawable.postafkiieksponati4);
        videoModel4.setEskponatTilte("adaiosjdasiodjoasd");
        videoList.add(videoModel4);



    }

}
