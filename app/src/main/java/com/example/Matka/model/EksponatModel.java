package com.example.Matka.model;

public class EksponatModel {

    int eksponatImage;
    String eksponatTitle;

    public EksponatModel(){}

    public int getEksponatImage() {
        return eksponatImage;
    }

    public void setEksponatImage(int eksponatImage) {
        this.eksponatImage = eksponatImage;
    }

    public String getEksponatTitle() {
        return eksponatTitle;
    }

    public void setEksponatTitle(String eksponatTitle) {
        this.eksponatTitle = eksponatTitle;
    }
}
