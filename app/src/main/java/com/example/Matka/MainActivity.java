package com.example.Matka;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.Matka.fragment.ContactFragment;
import com.example.Matka.fragment.HistoryFragment;
import com.example.Matka.fragment.HomeFragment;
import com.example.Matka.fragment.VideoFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_ID);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.home_icon_id);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (menuItem.getItemId()){
            case R.id.home_icon_id:
                ft.replace(R.id.frameID,HomeFragment.newInstance(),HomeFragment.TAG);
                ft.commit();
                return true;
            case R.id.video_icon_id:
                ft.replace(R.id.frameID, VideoFragment.newInstance(),VideoFragment.TAG);
                ft.commit();
                return true;
            case R.id.history_icon_id:
                ft.replace(R.id.frameID, HistoryFragment.newInstance(),HistoryFragment.TAG);
                ft.commit();
                return true;
            case R.id.contact_icon_id:
                ft.replace(R.id.frameID, ContactFragment.newInstance(),ContactFragment.TAG);
                ft.commit();
                return true;


        }
        return false;
    }

}
