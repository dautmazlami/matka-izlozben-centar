package com.example.Matka.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Matka.R;
import com.example.Matka.model.EksponatModel;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeAdapterViewHolder> {

    ArrayList<EksponatModel> eksponatList;
    LayoutInflater inflater;
    Context context;

    public HomeAdapter(Context context,ArrayList<EksponatModel> eksponatList) {
        this.eksponatList = eksponatList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public HomeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_eksponati,parent,false);
        return new HomeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapterViewHolder holder, int position) {

        EksponatModel eksponatModel = eksponatList.get(position);
        holder.eksponatImageView.setImageResource(eksponatModel.getEksponatImage());
        holder.eksponatTextView.setText(eksponatModel.getEksponatTitle());

    }

    @Override
    public int getItemCount() {
        return eksponatList.size();
    }

    public class HomeAdapterViewHolder extends RecyclerView.ViewHolder {

        ImageView eksponatImageView;
        TextView eksponatTextView;

        public HomeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            eksponatImageView = itemView.findViewById(R.id.eksponati_image_view);
            eksponatTextView = itemView.findViewById(R.id.eksponati_text_view);
        }
    }
}
